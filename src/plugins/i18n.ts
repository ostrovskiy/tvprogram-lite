import { createI18n } from 'vue-i18n'
import messages from '~/i18n/messages'
import { getLocale } from '~/utils/localeUtil'

export const i18n = createI18n({
  legacy: false,
  locale: getLocale(),
  fallbackLocale: 'en',
  messages,
})
