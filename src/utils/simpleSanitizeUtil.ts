export function sanitizeText(inputHtml: string): string | undefined {
  if (inputHtml) return inputHtml.replace(/<[^>]*>/g, '')
}
