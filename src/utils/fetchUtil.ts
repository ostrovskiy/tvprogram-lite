import { externalBaseUrl } from '~/consts/reqParams'
import { useAuthStore } from '~/store/auth'

export async function $fetch<T>(
  url: string,
  options: {
    method: string
    headers?: Record<string, any>
    params?: Record<string, any>
    query?: Record<string, any>
  },
  useBaseUrl?: 'external',
  authReq?: 'authorized'
): Promise<T> {
  const baseUrl = useBaseUrl ? externalBaseUrl : ''

  const headers = options.headers ? new Headers(options.headers) : new Headers()

  let fullUrl = `${baseUrl}${url}`

  if (options.query) {
    const queryString = new URLSearchParams(
      options.query as Record<string, string>
    ).toString()
    if (queryString) {
      fullUrl += `?${queryString}`
    }
  }

  if (authReq) {
    const token = useAuthStore().token
    if (token) {
      headers.append('X-Auth-Token', token)
    } else new Error('Token was not provided')
  }

  const requestOptions: RequestInit = {
    method: options.method,
    headers,
    body: options.params
      ? new URLSearchParams(options.params).toString()
      : undefined,
  }

  const response = await fetch(fullUrl, requestOptions)

  if (!response.ok) {
    throw new Error('Network response error')
  }

  return response.json() as Promise<T>
}
