import { DateTime } from 'luxon'
import type { TimezoneInterface } from '~/types/timezone'
import { i18n } from '~/plugins/i18n'

const WEEKDAY_MONTH_DATE_TIME: Intl.DateTimeFormatOptions = {
  weekday: 'long',
  month: 'short',
  day: 'numeric',
  hour: '2-digit',
  minute: '2-digit',
}

function luxonParseDate(
  date: string | Date,
  timezone: TimezoneInterface
): DateTime {
  if (typeof date === 'string') {
    return DateTime.fromISO(date, { zone: timezone })
  } else if (date instanceof Date) {
    return DateTime.fromJSDate(date, { zone: timezone })
  } else {
    throw new Error('Invalid date format')
  }
}

function luxonDateWithLocale(
  date: Date,
  timezone: TimezoneInterface
): DateTime {
  const { locale } = i18n.global

  return DateTime.fromJSDate(date, { zone: timezone }).setLocale(locale.value)
}

function combainDates(
  start: string,
  end: string,
  currentDate: Date,
  timezone: TimezoneInterface
) {
  return {
    startDate: luxonParseDate(start, timezone),
    endDate: luxonParseDate(end, timezone),
    currentDateTime: luxonParseDate(currentDate, timezone),
  }
}

export function calculateDate(substract = 0) {
  const currentDate = new Date()
  currentDate.setDate(currentDate.getDate() - substract)
  return currentDate
}

export function formatDate(
  date: Date,
  timezone: TimezoneInterface,
  density?: 'short' | 'medium' | 'time'
): string {
  const luxonDateTime = luxonDateWithLocale(date, timezone)

  if (density === 'short') {
    return luxonDateTime.toFormat('d LLL')
  } else if (density === 'time') {
    return luxonDateTime.toLocaleString(DateTime.TIME_SIMPLE)
  } else {
    return luxonDateTime.toLocaleString(WEEKDAY_MONTH_DATE_TIME)
  }
}

export function getDayRange(
  date: Date,
  timezone: TimezoneInterface
): { 'start_time[gt]': string; 'end_time[lt]': string } {
  const luxonDateTime = luxonDateWithLocale(date, timezone)

  const startOfDay = luxonDateTime.startOf('day')

  const startOfNextDay = startOfDay.plus({ days: 1 })

  const startUTC = startOfDay.toUTC(0)
  const endUTC = startOfNextDay.toUTC(0)

  const startISO = startUTC.toISO()
  const endISO = endUTC.toISO()

  return {
    'start_time[gt]': startISO || '',
    'end_time[lt]': endISO || '',
  }
}

export function isToday(date: Date, timezone: TimezoneInterface): boolean {
  const luxonDateTime = luxonDateWithLocale(date, timezone)
  const luxonCurrentDate = luxonDateWithLocale(calculateDate(), timezone)

  return luxonDateTime.hasSame(luxonCurrentDate, 'day')
}

export function dateWithOffset(
  date: Date,
  timezone: TimezoneInterface,
  offsetDays: number
): Date {
  const luxonDateTime = luxonDateWithLocale(date, timezone)

  const modifiedDateTime = luxonDateTime.plus({ days: offsetDays })

  return modifiedDateTime.toJSDate()
}

export function onAir(
  start: string,
  end: string,
  currentDate: Date,
  timezone: TimezoneInterface
): boolean {
  const dates = combainDates(start, end, currentDate, timezone)

  return (
    dates.currentDateTime >= dates.startDate &&
    dates.currentDateTime <= dates.endDate
  )
}

export function airProgress(
  start: string,
  end: string,
  currentDate: Date,
  timezone: TimezoneInterface
): number {
  const dates = combainDates(start, end, currentDate, timezone)

  const totalDuration = dates.endDate.diff(dates.startDate)

  const elapsedDuration = dates.currentDateTime.diff(dates.startDate)

  const progress =
    (elapsedDuration.as('milliseconds') / totalDuration.as('milliseconds')) *
    100

  return progress
}
