import { StateTree } from 'pinia'
import { ProgramInterface } from '~/types/program'
import { CachedProgramsInterface } from '~/types/cache'

export function restoreCache(value: string): StateTree {
  const parsed = JSON.parse(value)
  const result = new Map<string, CachedProgramsInterface>()

  for (const [key, cacheItem] of Object.entries(parsed)) {
    const typedCacheItem = cacheItem as CachedProgramsInterface
    typedCacheItem.expires = new Date(typedCacheItem.expires)
    result.set(key, typedCacheItem)
  }

  return { cachedPrograms: result }
}

export function saveCache(value: StateTree): string {
  const serialized: {
    [key: string]: { expires: string; value: ProgramInterface[] }
  } = {}

  for (const [key, cacheItem] of value.cachedPrograms.entries()) {
    serialized[key] = {
      expires: cacheItem.expires.toISOString(),
      value: cacheItem.value,
    }
  }

  return JSON.stringify(serialized)
}
