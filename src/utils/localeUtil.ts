import { isRef } from 'vue'
import type { AvaliableLocales } from '~/types/locales'
import messages from '~/i18n/messages'
import { i18n } from '~/plugins/i18n'

const locales: AvaliableLocales[] = Object.keys(messages) as AvaliableLocales[]

export function setLocale(localeValue: AvaliableLocales): AvaliableLocales {
  if (isRef(i18n.global.locale)) i18n.global.locale.value = localeValue
  localStorage.setItem('locale', localeValue)
  return localeValue
}

export function getLocale(fallback: AvaliableLocales = 'en'): AvaliableLocales {
  const storedLocale =
    (localStorage.getItem('locale') as AvaliableLocales) ||
    navigator.language.substring(0, 2)

  if (locales.includes(storedLocale)) {
    return storedLocale as AvaliableLocales
  }

  return fallback
}
