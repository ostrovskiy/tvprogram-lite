export const externalBaseUrl = 'https://discovery-ams.sotalcloud.com/sdp/v2'

export const tokenRequestParams = {
  method: 'POST',
  headers: new Headers({
    'Content-Type': 'application/x-www-form-urlencoded',
  }),
  params: {
    grant_type: 'client_credentials',
    client_id: 'cloud:mometu_ottweb_device',
    device_id: 'guest',
  },
}
