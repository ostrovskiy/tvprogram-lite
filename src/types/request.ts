export interface ProgramsRequestInterface {
  'live.id[eq]'?: number
  'start_time[gt]'?: string
  'end_time[lt]'?: string
}
