export interface ProgramInterface {
  id: number
  start_time: string
  end_time: string
  description: string
  name: string
  live_id: number
}
