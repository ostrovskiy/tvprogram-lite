import messages from '~/i18n/messages'

export type AvaliableLocales = keyof typeof messages
