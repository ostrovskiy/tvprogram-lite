import { ProgramInterface } from '~/types/program'

export interface CachedProgramsInterface {
  expires: Date
  value: ProgramInterface[]
}
