import type { VDataTable } from 'vuetify/lib/labs/components.mjs'

type UnwrapReadonlyArrayType<A> = A extends Readonly<Array<infer I>>
  ? UnwrapReadonlyArrayType<I>
  : A
type DT = InstanceType<typeof VDataTable>
export type DataTableHeader = UnwrapReadonlyArrayType<DT['headers']>
