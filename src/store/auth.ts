import { defineStore } from 'pinia'
import type { TokenInterface } from '~/types/token'
import { tokenRequestParams } from '~/consts/reqParams'
import { $fetch } from '~/utils/fetchUtil'

export const useAuthStore = defineStore('authentication', {
  state: () => ({
    token: undefined as string | undefined,
  }),

  actions: {
    async getToken() {
      const response: TokenInterface = await $fetch(
        '/authorize',
        tokenRequestParams,
        'external'
      )
      this.token = response.data
    },
  },
})
