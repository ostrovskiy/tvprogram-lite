import { defineStore } from 'pinia'
import type { ChannelInterface } from '~/types/channel'
import type { ProgramInterface } from '~/types/program'
import type { ProgramsRequestInterface } from '~/types/request'
import { CachedProgramsInterface } from '~/types/cache'
import { restoreCache, saveCache } from '~/utils/cacheSerializerUtil'
import { $fetch } from '~/utils/fetchUtil'

const expiresAfterHours = 1

export const useChannelsStore = defineStore('channels', {
  state: () => ({
    channels: [] as ChannelInterface[],
    channelId: undefined as number | undefined,
    channelPrograms: [] as ProgramInterface[],
    cachedPrograms: new Map<string, CachedProgramsInterface>(),
  }),

  getters: {
    channelsGetter: (state): ChannelInterface[] => {
      return state.channels
    },
    chanelGetter: (state): ChannelInterface | undefined => {
      return state.channels.find((c) => c.id === state.channelId)
    },
  },

  actions: {
    setChannelId(id: number) {
      this.channelId = id
    },

    async getChannels() {
      const response: ChannelInterface[] = await $fetch(
        '/fixtures/channels.json',
        {
          method: 'GET',
        }
      )
      this.channels = response
    },

    async getPrograms(payload: ProgramsRequestInterface) {
      const cacheKey = `${payload['live.id[eq]']}-${payload['start_time[gt]']}-${payload['end_time[lt]']}`

      const cached = this.getFromCache(cacheKey)

      if (cached) {
        this.channelPrograms = cached
      } else {
        const response: { data: ProgramInterface[] } = await $fetch(
          '/live-events',
          {
            method: 'GET',
            query: payload,
          },
          'external',
          'authorized'
        )
        this.channelPrograms = response.data
        this.addToCache(cacheKey, response.data)
      }
    },

    // Можно сделать более сложный алгоритм кеширования с расширением скоупа дат и динамической подгрузкой только недостающего содержимого,
    // но для текущего проекта это является избыточным по времени

    getFromCache(key: string): ProgramInterface[] | null {
      const cachedItem = this.cachedPrograms.get(key)

      if (cachedItem && cachedItem.expires > new Date()) {
        return cachedItem.value
      } else {
        return null
      }
    },

    addToCache(key: string, value: ProgramInterface[]) {
      const expireDate = new Date()

      expireDate.setHours(expireDate.getHours() + expiresAfterHours)

      const cacheValue = {
        expires: expireDate,
        value,
      }

      this.cachedPrograms.set(key, cacheValue)
    },
  },
  persist: {
    serializer: {
      deserialize: restoreCache,
      serialize: saveCache,
    },
    storage: sessionStorage,
    paths: ['cachedPrograms'],
  },
})
