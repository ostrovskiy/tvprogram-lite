import { defineStore } from 'pinia'
import type { TimezoneInterface } from '~/types/timezone'
import { $fetch } from '~/utils/fetchUtil'
import { calculateDate } from '~/utils/dateUtil'

let intervalId: NodeJS.Timeout | undefined

export const useClockStore = defineStore('clock', {
  state: () => ({
    systemDate: calculateDate() as Date,
    selectedDate: calculateDate() as Date,
    timezone: 'UTC' as TimezoneInterface,
    timezones: [] as TimezoneInterface[],
  }),

  getters: {
    timezonesGetter: (state): TimezoneInterface[] => {
      return state.timezones
    },
  },

  actions: {
    async getTimezones() {
      const response: TimezoneInterface[] = await $fetch(
        '/fixtures/timezones.json',
        {
          method: 'GET',
        }
      )
      this.timezones = response
    },

    setTimezone(timezone: TimezoneInterface) {
      this.timezone = timezone
    },

    setSelectedDate(date: Date) {
      this.selectedDate = date
    },

    updateDate() {
      this.systemDate = calculateDate()
    },

    startIntegratedClock() {
      intervalId = setInterval(this.updateDate, 1000)
    },

    stopIntegratedClock() {
      clearInterval(intervalId)
    },
  },
})
