import { createApp } from 'vue'
import App from './App.vue'

// Styles
import '~/assets/styles.scss'

// Plugins
import { pinia } from '~/plugins/pinia'
import { i18n } from '~/plugins/i18n'
import { vuetify } from '~/plugins/vuetify'

createApp(App).use(pinia).use(i18n).use(vuetify).mount('#app')
