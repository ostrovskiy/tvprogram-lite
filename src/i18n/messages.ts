import { ru, en } from 'vuetify/locale'
import type { LocaleMessages } from 'vue-i18n'

export default {
  ru: {
    $lang: 'Русский',
    $vuetify: ru,
    app: {
      title: 'TвПрограмма: Lite',
      channel: 'Канал',
      timezone: 'Часовой пояс',
      program: 'Передача',
      startTime: 'Начало',
      endTime: 'Конец',
      onAir: 'В эфире',
      loading: 'Подготовка данных...',
      tableProgramsEmpty: 'Укажите канал для отображения ТВ программы',
      programsTable: {
        virtual: 'Виртульная(экспериметнально)',
        static: 'Статичная',
        table: 'таблица',
        name: 'Название',
        description: 'Описание',
        start_time: 'Начало',
        end_time: 'Конец',
        empty: 'Доступных данных нет',
      },
    },
  },
  en: {
    $lang: 'English',
    $vuetify: en,
    app: {
      title: 'TvProgram: Lite',
      channel: 'Channel',
      timezone: 'Timezone',
      program: 'Program',
      startTime: 'StartTime',
      endTime: 'StartTime',
      onAir: 'On Air',
      loading: 'Gathering data...',
      tableProgramsEmpty: 'Set Channel, to see TV program',
      programsTable: {
        virtual: 'Virtual(experimental)',
        static: 'Static',
        table: 'table',
        name: 'Name',
        description: 'Description',
        start_time: 'Start Time',
        end_time: 'End Time',
        empty: 'No data avaliable',
      },
    },
  },
} as LocaleMessages<Record<string, any>>
