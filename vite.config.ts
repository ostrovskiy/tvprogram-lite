import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import Inspect from 'vite-plugin-inspect'

export default defineConfig({
  plugins: [vue(), Inspect()],
  resolve: {
    alias: {
      '~/': `${process.cwd()}/src/`,
    },
  },
})
